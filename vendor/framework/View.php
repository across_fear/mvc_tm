<?php

namespace Framework;


class View
{

    public function render($template, $content_page, $data = null){

        if(is_array($data))

        {

            extract($data);
            
        }

        require_once ROOT . '/appllication/views/' . $template;
    }

}