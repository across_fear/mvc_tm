<?php

namespace Framework;

class Route
{
    static function start(){

        $defaultClass = 'Default';
        $defaultAction = 'index';

        $url = $_SERVER['REQUEST_URI'];
        $route = explode('/', $url);
        $args = array_splice($route, 3);

        if($route[1]){
            $defaultClass = $route[1];
        }

        if(isset($route[2])){
            $defaultAction = $route[2];
        }
  
        $controllerName = ucfirst($defaultClass) . 'Controller';
        $actionName = $defaultAction . 'Action';
        $controller = new $controllerName();

        if ($url != REGISTER and $url != LOGIN) {
            OAuth::checkUserSession();
        }

        if(method_exists($controller, $actionName)){

            call_user_func_array(array($controller, $actionName), $args);
        }

    }

    static function ErrorPage404(){
        $host = 'http://' . $_SERVER['HTTP_HOST'] . '/';
        header('HTTP/1.1 404 Not Found');
        header('Status: 404 Not Found');
        header('Location:' . $host . '404');
    }

}