<?php
use Framework\Model;

class Comment extends Model
{
    private $tableName = 'comment';

    public function getCommentsByPostId($postId)
    {
        $sql = "SELECT c.*,u.id, u.first_name, u.last_name, pi.path , pi.main_photo_id
        FROM {$this->tableName} AS c  
        INNER JOIN user AS u ON c.author_id = u.id
        LEFT JOIN profile_image AS pi ON c.author_id = pi.author_id AND pi.main_photo_id = 1
        WHERE c.post_id=?
        ORDER BY c.created_at DESC
        ";

        $query = $this->connect->prepare($sql);
        $query->execute([$postId]);

        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public function save($data)
    {
        $sql = "INSERT INTO {$this->tableName} (author_id, post_id, text)  VALUES (?, ?, ?) ";
        $query = $this->connect->prepare($sql);
        $result = $query->execute(
            [
                $data['commentAuthorId'],
                $data['postId'],
                $data['commentText']
            ]
        );
        return $result;
    }
}