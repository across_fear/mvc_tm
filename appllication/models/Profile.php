<?php


use \Framework\Model;

class Profile extends Model

{

	private $tableName = 'post';

/** Get Info for profile
    * with u - user(u.first_name, u.last_name)
    * with p - post(p.author, p.id)
    * with pi - profile_image(pi.id, pi.path, pi.main_photo_id)
*/
    public function getAll()

    {

        $sql = "SELECT u.*,pi.id AS picture_id, pi.path
        FROM user AS u 
        LEFT JOIN profile_image AS pi ON u.id = pi.author_id AND pi.main_photo_id = '1' 
        WHERE u.id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }


//  Get posts by id for edit or something
    public function getById($id)

    {
        $sql = "SELECT *,p.id FROM $this->tableName AS p 
        INNER JOIN user AS u ON u.id = p.author
        WHERE u.id = p.author
        ";
        $query = $this->connect->prepare($sql);
        // if execute - conert string id to array
        $query->execute(['id']);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;

    }

// Get one post of user in profile
    public function getPosts() 

  {
       $sql = "SELECT id, title, text, created_at FROM $this->tableName 
         WHERE author = ? AND status = 0
         ORDER BY id DESC
        ";
        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;

  }
// Get comments by id for user 
    public function getComments()
    {
        $sql = "SELECT id, text, created_at FROM comment
         WHERE author_id = ? AND status = 0
        ";
        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        
        return $result;
    }
    
// Virtual delete post by id (simple) if (post.status = 1) post will be deleted 

    public function deleteById($id)

    {

        $query = $this->connect->prepare("UPDATE $this->tableName SET status = '1' WHERE id = $id");
        $query->execute();

    }

// Get avatar by id for user 
    public function getAvatarByid()

    {


        $sql = "SELECT u.*,pi.id AS picture_id, pi.path
        FROM user AS u 
        LEFT JOIN profile_image AS pi ON u.id = pi.author_id
        WHERE u.id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

// Change main_photo_id = 0 in db, by id user
    public function changeMain($img)

    {

        $db_add = "UPDATE profile_image AS pi 
        SET main_photo_id = '0'
        WHERE author_id = ?";
      
        $query = $this->connect->prepare($db_add);
        $query->execute([$_SESSION['user']['id']]);

    }

// Update photo by id
    public function updateAvatarById($img)

    {

        $db_add = "UPDATE profile_image AS pi 
        SET main_photo_id = '1'
        WHERE pi.id = $img";
      
        $query = $this->connect->prepare($db_add);
        $query->execute([$_SESSION['user']['id']]);
    }

// SET for edit user
    public function getUserById()

    {

        $sql = "SELECT * FROM user WHERE user.id = ?";

        $query = $this->connect->prepare($sql);
        $query->execute([$_SESSION['user']['id']]);
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }
// update user by id
    public function updateUser($user)

    {

        $db_add = "UPDATE user SET
        first_name = '{$user['update_first_name']}',
        last_name = '{$user['update_last_name']}',
        password = '{$user['update_password']}',
        birthday = '{$user['update_birthday']}'
        WHERE user.id=?";
      
        $query = $this->connect->prepare($db_add);
        $query->execute([$_SESSION['user']['id']]);

    }


}