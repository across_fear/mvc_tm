<?php

use Framework\Model;

class Post extends Model
{

   private $tableName = 'post';

    public function pagination($result_per_page)

    {   

        // prepare for count posts
        $sql = $this->connect->prepare("SELECT id FROM $this->tableName " );
        $sql->execute();
        // take num of Pages 
        $number_of_results = $sql->rowCount();
        // do result of ceil in variable
        $number_of_page = ceil($number_of_results/$result_per_page);


        return $number_of_page;

    }
// Get all Posts from Db with post.status = 0 
   public function getAll($result_per_page,$this_page_first_result)

    {
        $sql = "SELECT {$this->tableName}.*, user.first_name, user.last_name, cat.name, i.path AS post_image 
          FROM {$this->tableName} 
          INNER JOIN user ON {$this->tableName}.author = user.id 
          INNER JOIN post_image AS i ON {$this->tableName}.id = i.post_id
          LEFT JOIN category AS cat ON post.category_id = cat.id 
          WHERE post.status = 0
          ORDER BY created_at DESC LIMIT $this_page_first_result,$result_per_page";

        $query = $this->connect->prepare($sql);
        $query->execute();
        $result = $query->fetchAll(PDO::FETCH_ASSOC);

        return $result;
    }

// Get one post from Db 
    // with u - user(u.first_name, u.last_name)
    // with i - image(i.path, i.post_id)
    // with p - post(p.author, p.id)
    public function getById($postId)

    {
        $sql = "SELECT p.*, u.first_name, u.last_name, i.path AS post_image 
        FROM {$this->tableName} AS p 
        INNER JOIN user AS u ON p.author = u.id 
        INNER JOIN post_image AS i ON p.id = i.post_id

        WHERE p.id=?";

        $query = $this->connect->prepare($sql);
        // if execute - conert string postId to array
        $query->execute([$postId]);
        $result = $query->fetch(PDO::FETCH_ASSOC);

        return $result;

    }


    //save post to DB

    public function save($data)
    {
        $sql = "INSERT INTO {$this->tableName} (title, text, description, author) VALUES (?, ?, ?, ?)";
        $query = $this->connect->prepare($sql);
        $query->execute([
            $data['postTitle'],
            $data['postText'],
            $data['postDescription'],
            $data['postAuthorId'],
        ]);
        $postId = $this->connect->lastInsertId();

        return $postId;
    }


// Try edition , get post from DB by ID

    public function getPostById($id)

    {

        $query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE id = ?" );
        $query->execute([$id]);
        $user = $query->fetchAll(PDO::FETCH_ASSOC);

        return $user;

    }

// edit post - > Throw array with $user from form, and uppdate after save on DB

    public function editPost($id, $user)

    {


        $db_add = "UPDATE $this->tableName SET
        title = '{$user['title']}',
        text = '{$user['text']}',
        description = '{$user['description']}',
        author = '{$user['author']}',
        status = '{$user['status']}'

        WHERE id={$id}";
      
        $query = $this->connect->prepare($db_add);
        $query->execute();

    }

// Virtual delete post by id (simple) if (post.status = 1) post will be deleted 

    public function deleteById($id)

    {

        $query = $this->connect->prepare("UPDATE $this->tableName SET status = '1' WHERE id = $id");
        $query->execute();

    }


// Add +1 view from post do Db when user open /get/id 

    public function updateViews($postId)

{

        $query = $this->connect->prepare("UPDATE $this->tableName SET views = views + 1 WHERE id = ?");
        $query->execute([$postId]);

}


}