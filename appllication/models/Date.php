<?php

use Framework\Model;

class Date extends Model
{

	private $tableName = 'users_date';
	private $table2 = 'datetry';

	public function pagination($result_per_page)

	{	

		// prepare for count posts
		$sql = $this->connect->prepare("SELECT id FROM $this->tableName " );
		$sql->execute();
		// take num of Pages 
		$number_of_results = $sql->rowCount();
		// do result of ceil in variable
		$number_of_page = ceil($number_of_results/$result_per_page);


		return $number_of_page;

	}

	public function getAll($result_per_page,$this_page_first_result)
	
	{

		// Starting limit number
		$query = $this->connect->prepare("SELECT * FROM $this->tableName  LIMIT $this_page_first_result,$result_per_page");
		$query->execute();
		$users = $query->fetchAll(PDO::FETCH_ASSOC);

		return $users;
	}





public function getUserById($id)

	{

		$query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE id = ?" );
		$query->execute([$id]);
		$date = $query->fetchAll(PDO::FETCH_ASSOC);

		return $date;

	}

public function updateByid($id, $user)

	{

		$query = $this->connect->prepare("UPDATE $this->tableName SET 
			first_name = {$user['first_name']},
			last_name = {$user['last_name']},
			first_name = {$user['date']},
			first_name = {$user['birthday']}
			WHERE id = {$id}
			");
			$query = execute();		
	}

public function editUser($id, $param)

	{

	    $db_add = "UPDATE $this->tableName SET
	    first_name = '{$param['update_first_name']}',
	    last_name = '{$param['update_last_name']}',
	    email = '{$param['update_email']}',
	    birthday = '{$param['update_birthday']}',
	    status = '{$param['update_status']}'
	    WHERE id={$id}";
	  
	    $query = $this->connect->prepare($db_add);
	    $query->execute();

	}


public function deleteById($id)

	{

		$query = $this->connect->prepare("UPDATE $this->tableName SET status = '1' WHERE id = $id");
        $query->execute();

	}


public function save($user)
    {
    	$sql = "INSERT INTO $this->tableName (
    		first_name,
    		last_name,
    		date,
    		time) 
    		VALUES (?,?,?,?)";

		$query = $this->connect->prepare($sql);
        $query->execute( 

        	[$user['first_name'], 
             $user['last_name'], 
             $user['date'], 
             $user['time']]);

    }

public function showDel(){

		$query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE status = 1 ");
		$query->execute();
		$users = $query->fetchAll(PDO::FETCH_ASSOC);

		return $users;
	}


public function dateTrySave($date)

	{


		$sql = "INSERT INTO $this->table2 (date) VALUES( ? )";
		$query = $this->connect->prepare($sql);
		$query = execute([$date['trydate']]);
		die(var_dump($this->table2));

	}


public function getViews()

{

	$query = $this->connect->prepare("SELECT * FROM post_views ");
	$query->execute();
	$views = $query->fetchAll(PDO::FETCH_ASSOC);

	return $views;

}

public function updateViews($id)

{


	$query = $this->connect->prepare("UPDATE post_views SET views = views + 1 WHERE id = ?");
    $query->execute([$id]);

}

}