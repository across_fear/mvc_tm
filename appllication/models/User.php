<?php


use \Framework\Model;

class User extends Model

{
	 
	private $tableName = 'user';
	// Get all users from DB
	public function getAll()
	
	{

		$query = $this->connect->prepare("
		SELECT u.id,u.role_id,u.first_name,u.last_name,u.email,r.role_name
		FROM $this->tableName AS u 
		LEFT JOIN role_of_user AS r ON u.role_id = r.role_id
	 	WHERE status = 0 
			");
		$query->execute();
		$users = $query->fetchAll(PDO::FETCH_ASSOC);

		return $users;

	}

	 public function getByEmail($email){
        $query = $this->connect->prepare("
        	SELECT u.*, pi.author_id, pi.path
        	FROM $this->tableName AS u 
        	LEFT JOIN profile_image AS pi ON u.id = pi.author_id AND pi.main_photo_id = '1' 
        	WHERE email=?");
        $query->execute([$email]);
        $user = $query->fetch(PDO::FETCH_ASSOC);

        return $user;
    }
    
	public function getUserById($id)

	{

		$query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE id = ?" );
		$query->execute([$id]);
		$user = $query->fetchAll(PDO::FETCH_ASSOC);

		return $user;

	}



	public function editUser($id, $user)

	{

	    $db_add = "UPDATE $this->tableName SET
	    first_name = '{$user['update_first_name']}',
	    last_name = '{$user['update_last_name']}',
	    email = '{$user['update_email']}',
	    password = '{$user['update_password']}',
	    birthday = '{$user['update_birthday']}',
	    status = '{$user['update_status']}'
	    WHERE id={$id}";
	  
	    $query = $this->connect->prepare($db_add);
	    $query->execute();

	}


	function deleteById($id)

	{

		$query = $this->connect->prepare("UPDATE $this->tableName SET status = '1' WHERE id = $id");
        $query->execute();

	}


    public function save($user)

    {
    	$sql = "INSERT INTO $this->tableName (
    		first_name,
    		last_name,
    		email,
    		password,
    		birthday) 
    		VALUES (?,?,?,?,?)";

		$query = $this->connect->prepare($sql);
        $query->execute( 

        	[$user['first_name'], 
             $user['last_name'], 
             $user['email'], 
             $user['password'], 
             $user['birthday']
             ]);
    }


	function showDel(){

		$query = $this->connect->prepare("SELECT * FROM $this->tableName WHERE status = 1 ");
		$query->execute();
		$users = $query->fetchAll(PDO::FETCH_ASSOC);

		return $users;
	}




}