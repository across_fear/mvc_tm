<?php

use Framework\Model;

class Image extends Model
{
    private $tableName = 'post_image';

    public function save($imagePath, $postId)
    {
        $sql = "INSERT INTO {$this->tableName} (
                path,
                post_id)
                VALUES (?, ?)";
        $query = $this->connect->prepare($sql);
        $query->execute([$imagePath, $postId]);

    }

        public function saveToProfile($imagePath)
    {
        $sql = "INSERT INTO profile_image (
                path,
                author_id)
                VALUES (?, ?)";
        $query = $this->connect->prepare($sql);
        $query->execute([$imagePath, $_SESSION['user']['id']]);

    }

}