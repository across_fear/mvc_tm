<?php
use Framework\Controller;

class CommentController extends Controller
{

    public function addAction()
    {
        if (isset($_POST['submit'])) {

            $commentModel = new Comment();
            $commentModel->save($_POST);


            $this->redirect(
                'PostController',
                'getAction',
                [$_POST['postId']]);
        }
    }
}