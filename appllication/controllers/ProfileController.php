<?php


use \Framework\Controller;

class ProfileController extends Controller

{
	public function getAction()

    {
        $postModel = new Profile();
        $post = $postModel->getAll();
        $path = $post['0']['path'];
        $_SESSION['user']['image'] = $path;
        $this->view->render(
            'layout.php',
            'profile/profile_view.php',
            [
                'post' => $post,
            ]
        );
    }

    public function postsAction()
    {
        $postModel = new Profile();
        $post = $postModel->getPosts();

        $this->view->render(
            'layout.php',
            'profile/profile_post.php',
            [
                'post' => $post,
            ]
        );
    }

    public function commentsAction()

    {

        $commentModel = new Profile();
        $comments = $commentModel->getComments();

        $this->view->render(
            'layout.php',
            'profile/profile_comments.php',
            [
                'comments' => $comments,
            ]
        );
    }

    public function avatarAction()
    {

        $postModel = new Profile();
        $post = $postModel->getAvatarByid();

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        // Load photo from imput
        ImageHelper::uploadProfile($_FILES['postPicture']);

            $this->redirect(
                'ProfileController',
                'avatarAction');
        } else {
 
            $this->view->render(
                'layout.php',
                'profile/profile_avatar.php',
                [
                    'post' => $post
                ]
            );
        }
    }

    public function changeAvatarAction()

    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $img = $_POST['img'];
            $userModel = new Profile();
            $users = $userModel->changeMain($img);
            $users = $userModel->updateAvatarById($img);

            header('Location: /profile/get');
        }

    }

    public function settingsAction()

    {

        $userModel = new Profile();
        $users = $userModel->getUserById();

        $this->view->render(
                'layout.php',
                'profile/settings.php',
                [
                    'users' => $users
                ]
            );

    }

    public function updateAction($id)

    {
        $user = $_POST['user'];
        $user['update_password'] = md5($user['update_password']);
    
        $userModel = new Profile();
        $userModel->updateUser($user);
        $this->redirect('Profile', 'settings');

    }

    public function deleteAction($id)

    {

        $model = new Profile();
        $posts = $model->deleteById($id);

        header('Location: /profile/posts');
    
    }


}