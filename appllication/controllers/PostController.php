<?php
use Framework\Controller;

class PostController extends Controller
{

// Published posts 



    public function postsAction()
    {


        $model = new Post();
        // Pagination edion 
        if(!isset($_GET['page'])){
            $page = '1';
        }else{
            $page = $_GET['page'];
        }
        $result_per_page = 5;
        $prev = $page - 1;
        $next = $page + 1;
        $this_page_first_result = ($page - 1)*$result_per_page;
        $number_of_page = $model->pagination($result_per_page);
        //valid for url, if Get['id'] > url get['id']  redirect to date / date 
        if($page > $number_of_page){
          $this->redirect('post', 'posts', ['?page=1']);
        }  
        //end pagination



        $posts = $model->getAll($result_per_page, $this_page_first_result);


        $this->view->render(
            'layout.php',
            'post/posts.php',
            [
                'posts' => $posts,
                'number_of_page' => $number_of_page,
                'prev' => $prev,
                'next' => $next,
                'page' => $page
            ]
        );
    }

// Get Posts by id and intergrate Comments in posts

    public function getAction($postId)
    {
        $postModel = new Post();
        $post = $postModel->getById($postId);
        $views = $postModel->updateViews($postId);
        $commentModel = new Comment();
        $comments = $commentModel->getCommentsByPostId($postId);

        $this->view->render(
            'layout.php',
            'post/post.php',
            [
                'post' => $post,
                'comments' => $comments,
            ]
        );
    }

// Save too DB Post And load ImageHelper(this class load files and save name of jpg to db)

    public function addAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $postModel = new Post();
            $postId = $postModel->save($_POST);

            ImageHelper::upload($_FILES['postPicture'], $postId);

            $this->redirect(
                'PostController',
                'getAction',
                [$postId]);
        } else {
            $this->view->render(
                'layout.php',
                'post/add_post.php',
                []
            );
        }
    }

// Throw All posts in table to admin(for uppdate post)

    public function showAction()

    {
        $model = new Post();
        // Pagination edion 
        if(!isset($_GET['page'])){
            $page = '1';
        }else{
            $page = $_GET['page'];
        }
        $result_per_page = 5;
        $prev = $page - 1;
        $next = $page + 1;
        $this_page_first_result = ($page - 1)*$result_per_page;
        $number_of_page = $model->pagination($result_per_page);
        //valid for url, if Get['id'] > url get['id']  redirect to date / date 
        if($page > $number_of_page){
          $this->redirect('post', 'posts', ['?page=1']);
        }  
        //end pagination

        $posts = $model->getAll($result_per_page,$this_page_first_result);

        $this->view->render(
            'layout.php',
            'post/show_post.php',
            [
                'posts' => $posts,
                'number_of_page' => $number_of_page,
                'prev' => $prev,
                'next' => $next,
                'page' => $page
            ]
        );

    }

// Take 1 post by id (for uppdate post)

    public function editAction($id)
    
    {

        $model = new Post();
        $posts = $model->getPostById($id);
        Permitions::RoleUrlBlockForUser();
        $this->view->render(
            'layout.php',
            'post/edit_post.php',
            [ 'posts' => $posts ] );
    }


// Uppdate post and throw to Db

    public function updateAction($id)

    {

        $model = new Post();
        $user = $_POST['user'];
        $user['title'] = addslashes($user['title']);
        $user['text'] = addslashes($user['text']);
        $user['description'] = addslashes($user['description']);
        $posts = $model->editPost($id, $user);
        if (isset($_POST)) {
            $this->redirect(
                'PostController',
                'showAction');

        } 


    }

// Virtual delete Post 

    public function deleteAction($id)

    {

        $model = new Post();
        $posts = $model->deleteById($id);

        header('Location: /post/show');
    
    }
}

// request response 