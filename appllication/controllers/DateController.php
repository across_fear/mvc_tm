<?php
use Framework\Controller;

class DateController extends Controller
{

	public function dateAction()

	{
		
		$model = new Date();
		Permitions::RoleUrlBlockForUser();
		// Pagination edion	
		if(!isset($_GET['page'])){
			$page = '1';
		}else{
			$page = $_GET['page'];
		}
		$result_per_page = 5;
		$prev = $page - 1;
		$next = $page + 1;
		$this_page_first_result = ($page - 1)*$result_per_page;
		$number_of_page = $model->pagination($result_per_page);
		//valid for url, if Get['id'] > url get['id']  redirect to date / date 
	    if($page > $number_of_page){
	   	  $this->redirect('date', 'date', ['?page=1']);
	    }  
		//end pagination
	

		// throw variable $reult_per_page and $this_page_first_result to model->getAll
		$users = $model->getAll($result_per_page, $this_page_first_result);
		
		$this->view->render('layout.php' , 'date/date_show.php',
			 [
		   	    'users' => $users,
			 	'number_of_page' => $number_of_page,
				'prev' => $prev,
				'next' => $next,
				'page' => $page
			]);

	}

	public function tryDateAction()

	{

		$model = new Date();

		$this->view->render('layout.php' , 'date/date_try.php');
	}





	public function editAction($id)

	{

		$model = new Date();
		$date = $model->getUserByID($id);

		$this->view->render('layout.php' , 'date/date_edit.php', ['date' => $date]);

	}
	
	public function updateAction($id)

	{

		$model = new User();
		$param = $_POST['param'];
		$model->editUser($id, $param);
		$this->view->render('layout.php', 'user/update_done.php');

	}


	public function deleteAction($id)

	{

		$model = new User();
		$user = $model->deleteById($id);
		$this->view->render('layout.php', 'date/date_show.php');
	
	}

	public function addAction()
	{
		if (isset($_POST['user'])) {

			$model = new User();

			$user = $_POST['user'];
			
			$model->save($user);

			$this->redirect('user', 'users');

		} else {
			$this->view->render('layout.php', 'date/add_user.php');
		}
	}



	public function show_deleteAction()
	
	{

		$model = new User();
		$users = $model->showDel();
		$this->view->render('layout.php' , 'date/show_delete.php', ['users' => $users]);

	}


	public function viewsAction()
	{

		$model = new Date();

		$views = $model->getViews();


		$this->view->render('layout.php', 'date/views.php', ['views' => $views] );

	}

		public function view_pageAction($id)
	{

		$model = new Date();
		$views = $model->updateViews($id);

			$this->redirect('date', 'views');

	}


}