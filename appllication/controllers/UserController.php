<?php


use \Framework\Controller;

class UserController extends Controller

{
	


    public function loginAction()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = $_POST['user'];
            $model = new User();
            $checkEmail = $model->getByEmail($user['email']);
            $trimmedUser = FormValidator::clean($user);;
			$errors = FormValidator::checkLoginForm($checkEmail, $trimmedUser);

            if (count($errors) > 0) {
                $this->view->render('user/login.php', null, ['errors' => $errors]);
            } else {
                $_SESSION['user'] = [
                	'id' => $checkEmail['id'],
                    'first_name' => $checkEmail['first_name'],
                    'last_name' => $checkEmail['last_name'],
                    'email' => $checkEmail['email'],
                    'role_id' => $checkEmail['role_id'],
                    'image' => $checkEmail['path']
                ];

                header("Location:/post/posts");
            }

        } else {

            $this->view->render('layout.php','user/login.php');
        }
    }

    public function logoutAction()
    {
        unset($_SESSION['user']);
        header("Location:/user/login");
    }




	public function usersAction()

	{

		$model = new User();
		$users = $model->getAll();
        Permitions::RoleUrlBlockForUser();
        
        $this->view->render('layout.php' , 'user/users.php', ['users' => $users]);
        

	}

	public function editAction($id)

	{

		$model = new User();
		$user = $model->getUserByID($id);
		$this->view->render('layout.php' , 'user/edit_page.php', ['users' => $user]);

	}
	
	public function updateAction($id)

	{

		$model = new User();
		$user = $_POST['user'];
		$user['update_password'] = md5($user['update_password']);
		$model->editUser($id, $user);

		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$this->view->render('layout.php', 'user/update_done.php');
		}
	}




	public function deleteAction($id)

	{

		$model = new User();
		$user = $model->deleteById($id);
        
		$this->view->render('layout.php', 'user/update_done.php');
	
	}

	public function addAction()
	{

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = $_POST['user'];
            $model = new User();
            $checkEmail = $model->getByEmail($user['email']);
            $trimmedUser = FormValidator::clean($user);
            $errors = FormValidator::checkAdminForm($checkEmail, $trimmedUser);

            if (count($errors) > 0) {

                $this->view->render('user/add_user.php', null, ['errors' => $errors]);

            } else {

                $user['password'] = md5($user['password']);
                $model->save($user);

                header("Location:/user/users");
            }

        } else {
            $this->view->render('user/add_user.php', null);
        }
	}



	public function show_deleteAction()
	
	{

		$model = new User();
		$users = $model->showDel();
		$this->view->render('layout.php' , 'user/show_delete.php', ['users' => $users]);

	}



    public function registerAction()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $user = $_POST['user'];
            $model = new User();
            $checkEmail = $model->getByEmail($user['email']);
            $trimmedUser = FormValidator::clean($user);
            $errors = FormValidator::checkRegisterForm($checkEmail, $trimmedUser);

            if (count($errors) > 0) {

                $this->view->render('user/register_form.php', null, ['errors' => $errors]);

            } else {

                $user['password'] = md5($user['password']);
                $model->save($user);

                $this->view->render('user/login_success.php', null);
            }

        } else {
            $this->view->render('user/register_form.php', null);
        }
    }



}