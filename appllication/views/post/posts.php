<h1 class="text-center" >Posts</h1>
 <?php if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] != 1)  { ?>
<!-- Add post -->
<a class="btn btn-default" href="/post/add" >Create post</a> 
<!-- Eddit post -->
<a class="btn btn-default" href="/post/show">Edit Posts</a>
<?php } ?>

<!-- foreach from PostController feach all Posts -->
<?php foreach($posts as $post) :?>

<div class="media well well-lg">

  <div class="media-left">
  <!-- Get post by id  -->
    <a href="/post/get/<?=$post['id'];?>">
    <!-- Image of Post  -->
       <img src="<?=$post['post_image'];?>" id="post-id" class="img-circle" alt="No Photo Loaded" width="150" height="150">
    </a>
  </div>
  <div class="media-body">
  <!--  ID TITLE of POST -->
      <h3> Post №: <?=$post['id']?><a href="/post/get/<?=$post['id'];?>"> <?=$post['title'];?></a></h3>
  <!-- When Post Created -->
      <span>Published: <?=$post['created_at'];?></span><br>
  <!-- Description of Post -->
      <p>  <?=$post['description'];?></p>
      <span class="btn btn-outline-info"><?=$post['name'] ?></span >
      <hr>
  <!-- href on eddit user page. and show message first name , last name -->
      <!-- <span>Author: <a href="/profile/get/<?=$post['author'];?>"><?=$post['first_name'] . ' ' . $post['last_name'];?></a></span> -->
      <span>Author: <?=$post['first_name'] . ' ' . $post['last_name'];?></span>
  <!-- show views of Post -->
      <span class="badge">Views: <?=$post['views'];?></span>

  </div>
  
</div>

<?php endforeach;?>

<!-- Pagination Nav -->
<nav aria-label="Page navigation">
  <ul class="pagination">

    <?php   

    if($page <= 1){
          echo '<li><a href="/post/posts/?page='. $prev .'" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a></li>';
        }

    for($page = 1; $page <= $number_of_page; $page++){
          echo '<li><a href="/post/posts/?page='. $page .'">' . $page . '</a></li>';
        }
        
    if($page >= $number_of_page){
          echo '<li><a href="/post/posts/?page='. $next .'" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
        }
    ?>

  </ul>
</nav>