<a href="/post/posts"><span class="glyphicon glyphicon-arrow-left"></span> back</a>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <h1><?=$post['title'];?></h1>
                <h1></h1>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Published: <?=$post['created_at'];?></span>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Author: <a href="/profile/get/<?=$post['author'];?>"><?=$post['first_name'] . ' ' . $post['last_name'];?></a>  </span>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <img src="<?=$post['post_image'];?>" id="post-id" class="img-rounded" alt="Cinque Terre" width="1000" height="350">
                <div class="col-md-10">
                <p><?=$post['text'];?></p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <span>Views: <?=$post['views'];?> </span>
            </div>
        </div>
    </div>
</div>
<hr>
<h3>Comments:</h3>


<?php if ($comments): ?>
    <?php foreach ($comments as $comment):?>

        <div class="row comment">
          <div class="media-left">
                <img class="media-object" src="<?=$comment['path'];?>" alt="No Profile Image" width="70" height="70">
          </div>
          <div class="media-body">
            <h5 class="media-heading"><b><?=$comment['first_name'] . ' ' . $comment['last_name'];?></b></h5>
            <span>Publish date: <?=$comment['created_at'];?></span>
                        <p><?=$comment['text'];?></p>
          </div>
        </div>

    <?php endforeach;?>
<?php else:?>
    <p>No comments yet :(</p>
<?php endif;?>
<hr>
<h3>Add comment</h3>
<hr>
 <div class="row">
    <div class="col-md-5">
        <form action="/comment/add" method="POST" class="form-group">

            <input type="text" hidden value="<?=$post['id'];?>" name="postId">

            <label>Author</label>

            <select class="form-control" name="commentAuthorId">
                   <option value="<?=$_SESSION['user']['id']?>"><?=$_SESSION['user']['first_name']?> <?=$_SESSION['user']['last_name']?></option>
            </select>
            <label></label>
            <textarea placeholder="Enter comment text" class="form-control" rows="6" name="commentText"></textarea>
            <br>
            <button type="submit" class="form-control btn btn-default" name="submit">Submit</button>
        </form>
    </div>
</div>
<hr> 