<h1 class="text-center"> Add post </h1>

<hr>


<div class="col-md-6 col-md-offset-3">

    <form action="/post/add" method="POST" class="form-group" enctype="multipart/form-data">
    
        <label>Author: </label>
        <select class="form-control" name="postAuthorId">
            <option value=" <?=$_SESSION['user']['id']?>"> <?=$_SESSION['user']['first_name']?>  <?=$_SESSION['user']['last_name']?>  </option>
        </select>

        <label>Title: </label>
        <input type="text" name="postTitle" class="form-control">

        <label>Text: </label>
        <textarea placeholder="Post text" name="postText" class="form-control" rows="8"></textarea>

         <label>Description: </label>
        <textarea placeholder="Description" name="postDescription" class="form-control" rows="6"></textarea>

        <label>Image</label>
        <input type="file" name="postPicture">
        <br>
        <button type="submit" name="submit" class="form-control">Submit</button>
    </form>
</div>