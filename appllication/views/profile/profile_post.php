<a href="/profile/get/<?=$_SESSION['user']['id']?>"><span class="glyphicon glyphicon-arrow-left"></span> back </a>

<!-- Add post -->
<a href="/post/add" ><button>Create new post</button></a> 

<div class="container-fluid">
  <div class="row">
	<!-- Right navbar  -->
    <div class="col-sm-3 col-lg-2 col-sm-push-9 col-lg-push-10">
      <nav class="navbar navbar-default navbar-fixed-side">
        <ul class="nav nav-pils nav-stacked nav-right">
	         <li><a href="/profile/get/<?=$_SESSION['user']['id']?>"><span class="glyphicon glyphicon-home"></span> Profile </a></li>
	         <li><a href="/profile/avatar/"><span class="glyphicon glyphicon-user"></span>  Avatar  </a></li>
		     <li><a href="/profile/comments/"><span class="glyphicon glyphicon-comment"></span> Comments  </a></li>
		     <li><a href="/profile/settings"> <span class="glyphicon glyphicon-pencil"></span> Settings </a></li>
	    </ul>
      </nav>
    </div>

<!-- normal collapsable navbar markup -->
  <div class="col-sm-9 col-lg-10 col-sm-pull-3 col-lg-pull-2">
      <div class="col-md-9">
		      <h1><?=$_SESSION['user']['first_name']?> <?=$_SESSION['user']['last_name']?></h1>
      </div>

<?php if($post): ?>
	<table class="table ">
		
		<thead>

			<tr>
				<th>Edit</th>
				<th>Title</th>	
				<th>Delete</th>	
			</tr>
		
		</thead>

		
		<tbody>
		
			<?php foreach ($post as $posts) :?> 
				<tr>
				<td><a href="/post/edit/<?=$posts['id']?>" class="glyphicon glyphicon-pencil">Edit</a> </td>
				<td><?=$posts['title']?></td>
				<td><a href="/profile/delete/<?=$posts['id']?>" class="glyphicon glyphicon-remove" ></a> </td>
				</tr>
		<?php endforeach; ?>
		<?php else:?>
	        <div class="col-md-9">
	        	<h4> Try to post something, posts are empty .... </h4>
		    </div>	
		<?php endif; ?>
		</tbody>

	</table>
  </div>

  </div>
</div>