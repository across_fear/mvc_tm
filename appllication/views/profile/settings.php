
<a href="/profile/get/<?=$_SESSION['user']['id']?>"><span class="glyphicon glyphicon-arrow-left"></span> back </a>
 
<!-- <?php if(isset($errors)): ?>
    <div class="errors">
            <?php foreach ($errors as $error) : ?>
                <div class="alert alert-danger">
                    <?=$error?>
                </div>
            <?php endforeach; ?>
    </div>
<?php endif; ?> -->

<?php foreach ($users as $user): ?>
<form method="POST" action="/profile/update/<?=$user['id']?>" style=" width: 50%;">
  <div class="form-group">
    <label for="fisrt_name">Name</label>
    <input type="text" class="form-control" name="user[update_first_name]" id="fisrt_name" placeholder="Name" value="<?=$user['first_name']?>">
  </div>

  <div class="form-group">
    <label for="last_name">Last Name </label>
    <input type="text" class="form-control" name="user[update_last_name]"id="last_name" placeholder="last name" value="<?=$user['last_name']?>">
  </div>


  <div class="form-group">
    <label for="password">Change password </label>
    <input type="password" class="form-control" name="user[update_password]">
  </div>


  <div class="form-group">
    <label for="birthday"> Birth date </label>
    <input type="text" class="form-control" name="user[update_birthday]" id="birthday" placeholder="birthday" value="<?=$user['birthday']?>">
  </div>

  <button type="submit" class="btn btn-default" name="editUser">Edit</button>
</form>
<?php endforeach; ?>