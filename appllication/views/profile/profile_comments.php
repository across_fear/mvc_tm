<a href="/profile/get/<?=$_SESSION['user']['id']?>"><span class="glyphicon glyphicon-arrow-left"></span> back</a>

<div class="container-fluid">
  <div class="row">
    <!-- Right navbar  -->
    <div class="col-sm-3 col-lg-2 col-sm-push-9 col-lg-push-10">
      <nav class="navbar navbar-default navbar-fixed-side">
        <ul class="nav nav-pils nav-stacked nav-right">
             <li><a href="/profile/get/<?=$_SESSION['user']['id']?>"><span class="glyphicon glyphicon-home"></span> Profile </a></li>
             <li><a href="/profile/avatar/"><span class="glyphicon glyphicon-user"></span>  Avatar  </a></li>
             <li><a href="/profile/posts/"><span class="glyphicon glyphicon-list-alt"></span> Post  </a></li>
             <li><a href="/profile/settings"> <span class="glyphicon glyphicon-pencil"></span> Settings </a></li>
        </ul>
      </nav>
    </div>

<!-- normal collapsable navbar markup -->
  <div class="col-sm-9 col-lg-10 col-sm-pull-3 col-lg-pull-2">

<h1> Your comments </h1>
<?php if($comments): ?>
<table class="table ">
    
    <thead>

        <tr>
            <th>id</th>
            <th>Text</th>  
            <th>Created At</th> 
        </tr>
    
    </thead>

    
    <tbody>

        <?php foreach ($comments as $comment) :?> 
            <tr>
            <!-- <td><a href="/post/edit/<?=$posts['id']?>" class="glyphicon glyphicon-pencil">Edit</a> </td> -->
            <td><?=$comment['id']?></td>
            <td><?=$comment['text']?></td>
            <td><?=$comment['created_at']?></td>
            <!-- <td><a href="/post/delete/<?=$posts['id']?>" class="glyphicon glyphicon-remove" ></a> </td> -->
            </tr>

        <?php endforeach; ?>
            <?php else:?>
            <div class="col-md-9">
                <h4> Try to comment something, posts are empty .... </h4>
            </div>  
        <?php endif; ?>
    </tbody>

</table> 
 </div>

  </div>
</div>