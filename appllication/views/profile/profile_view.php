<?php foreach ($post as $posts):?>

<!-- normal collapsable navbar markup -->
<div class="container-fluid">
  <div class="row">

    <div class="col-sm-3 col-lg-2 col-sm-push-9 col-lg-push-10">
      <nav class="navbar navbar-default navbar-fixed-side">
        <ul class="nav nav-pils nav-stacked nav-right">
         <li><a href="/profile/avatar/"><span class="glyphicon glyphicon-user"></span> Avatar  </a></li>
         <li><a href="/profile/posts/"><span class="glyphicon glyphicon-list-alt"></span> Posts  </a></li>
		     <li><a href="/profile/comments/"><span class="glyphicon glyphicon-comment"></span> Comments  </a></li>
         <li><a href="/profile/settings"> <span class="glyphicon glyphicon-pencil"></span> Settings </a></li>
		    </ul>
      </nav>
    </div>

  <!-- normal collapsable navbar markup -->
    <div class="col-sm-9 col-lg-10 col-sm-pull-3 col-lg-pull-2">
        <div class="col-md-3 col-sm-3">
          <!-- Image path -->
          <img src="<?=$posts['path'];?>" id="image-id" class="img-rounded" alt="Photo Dosn't Exists" width="200" height="200" >
        </div>

        <div class="col-md-9">
       
            <h2> <?=$posts['first_name']?> <?=$posts['last_name']?> </h2>
             
        </div>     
         <div class="col-md-9">
         <p><span>Your Email adress: <?=$posts['email']?></span></p>
         <span>Your birthdate: <?=$posts['birthday']?></span>
        </div>

    </div>
  </div>
</div>

<?php endforeach; ?>
