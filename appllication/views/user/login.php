<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/web/css/main.css">
    <title>Login</title>
</head>
<body>

<div class="container login-form">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-login">

                <div class="panel-body">
                    <?php if(isset($errors)): ?>
                        <div class="errors">
                            <?php foreach ($errors as $error) : ?>
                                <div class="alert alert-danger">
                                    <?=$error?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>

                <div class="row main">
                   <div class="panel-heading">
                      <div class="panel-title text-center">
                         <h1 class="title">Login</h1>
                         <hr />
                      </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <form id="login-form" action="/user/login" method="post">
                                <div class="form-group">
                                    <input type="email" name="user[email]" id="username" tabindex="1" class="form-control" placeholder="Email" value="">
                                </div>
                                <div class="form-group">
                                    <input type="password" name="user[password]" id="password" tabindex="2" class="form-control" placeholder="Password">
                                </div>
<!--                                <div class="form-group text-center">-->
<!--                                    <input type="checkbox" tabindex="3" class="" name="remember" id="remember">-->
<!--                                    <label for="remember"> Remember Me</label>-->
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-3">
                                            <input type="submit" name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Sign In">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="text-center">
<!--                                                <a href="https://phpoll.com/recover" tabindex="5" class="forgot-password">Forgot Password?</a>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>  <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-12">
                                    <span id="register-form-link">You can
                                        <a href="/user/register">Register</a> now.
                                    </span>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>