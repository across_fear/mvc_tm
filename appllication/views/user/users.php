<h1>Hello <?=$_SESSION['user']['first_name']?> </h1> 
 <?php if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] == 2)  { ?>
<a class="btn btn-default" href="add"> Add User </a>

<a class="btn btn-default" href="show_delete"> Show deleted users </a>
<?php  }?>
<table class="table">
	
	<thead>

		<tr>
			<th>Id</th>	
			<th>Role Id</th>	
			<th>Name</th>	
			<th>Email</th>	
			<th>Delete</th>	
		</tr>
	
	</thead>
	<tbody>
		<?php foreach ($users as $user) :?> 

			<tr>
			<td><?=$user['id']?></td>
			<td><?=$user['role_name']?></td>
			<td><span class="glyphicon glyphicon-pencil"></span> <a href="edit/<?=$user['id']?>"><?=$user['first_name']?> <?=$user['last_name']?> </a> </td>
			<td><?=$user['email']?></td>
			<td><a href="delete/<?=$user['id']?>" class="glyphicon glyphicon-remove" ></a> </td>
			</tr>

		<?php endforeach; ?>
	
	</tbody>

</table>