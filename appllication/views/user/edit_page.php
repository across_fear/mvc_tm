
 <a href="/user/users">  Home page </a>
 
<?php if(isset($errors)): ?>
    <div class="errors">
            <?php foreach ($errors as $error) : ?>
                <div class="alert alert-danger">
                    <?=$error?>
                </div>
            <?php endforeach; ?>
    </div>
<?php endif; ?>


<?php foreach ($users as $user) :?>
<form method="POST" action="/user/update/<?=$user['id']?>" style=" width: 50%;">
  <div class="form-group">
    <label for="fisrt_name">Name</label>
    <input type="text" class="form-control" name="user[update_first_name]" id="fisrt_name" placeholder="Name" value="<?=$user['first_name']?>">
  </div>

  <div class="form-group">
    <label for="last_name">Last Name </label>
    <input type="text" class="form-control" name="user[update_last_name]"id="last_name" placeholder="last name" value="<?=$user['last_name']?>">
  </div>

  <div class="form-group">
    <label for="email"> email </label>
    <input type="email" class="form-control" name="user[update_email]" id="email" placeholder="email" value="<?=$user['email']?>">
  </div>

  <div class="form-group">
    <label for="password">Change password </label>
    <input type="text" class="form-control" name="user[update_password]" id="password" placeholder="Change password" ">
  </div>

  <div class="form-group">
    <label for="birthday"> Birth date </label>
    <input type="text" class="form-control" name="user[update_birthday]" id="birthday" placeholder="birthday" value="<?=$user['birthday']?>">
  </div>

    <div class="form-group">
		<label for="birthday"> Status
		<input type="text" name="user[update_status]" id="name" value="<?=$user['status']?>"> </input> </label>
  </div>

  <button type="submit" class="btn btn-default" name="editUser">Edit</button>
</form>
<?php endforeach; ?>