<h1> Users </h1>

<a class="btn btn-default" href="add"> Add User </a>

<a class="btn btn-default" href="show_delete"> Show deleted users </a>



<table class="table">
  
  <thead>
  
    <tr>
      <th>Name</th> 
      <th>Years</th>  
      <th>Time</th>  
    </tr>
  
  </thead>

  
  <tbody>
  <!-- foreach on $user  -->
    <?php foreach ($users as $user) :?> 
      <tr>
      <td><a href="edit/<?=$user['id']?>">Edit </a> | <?=$user['id']?> <?=$user['first_name']?> <?=$user['last_name']?></td>
      <!-- function for date => years -->
      <td><?php 
        $startDate = $user['date'];
        $diff =  floor((time() - strtotime($startDate))  / 60 / 60 / 24) ;
        $years = ceil($diff/365);
        echo $years ?>
      </td>
      <td><?=$user['time']?></td>
      <td><a href="delete/<?=$user['id']?>">Delete</a> </td>
      </tr>

    <?php endforeach; ?>

  </tbody>

</table>


<!-- Pagination Nav -->
<nav aria-label="Page navigation">
  <ul class="pagination">

    <?php   

    if($page > 1){
          echo '<li><a href="/date/date/?page='. $prev .'" aria-label="Previous"> <span aria-hidden="true">&laquo;</span> </a></li>';
        }

    for($page = 1; $page <= $number_of_page; $page++){
          echo '<li><a href="/date/date/?page='. $page .'">' . $page . '</a></li>';
        }
        
    if($_GET['page'] = 1 && $_GET['page'] < $number_of_page){
          echo '<li><a href="/date/date/?page='. $next .'" aria-label="Next"> <span aria-hidden="true">&raquo;</span> </a></li>';
        }
    ?>

  </ul>
</nav>


