<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">


    <link rel="stylesheet" href="/web/css/main.css">
    <link rel="stylesheet" href="/web/css/avatar.css">
    <title>Home page</title>
</head>
<body>

<div class="header">

    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/post/posts">Blog</a>

            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
            <?php if(isset($_SESSION['user']) && $_SESSION['user']['role_id'] != 1)  { ?>
                    <li class="nav action"><a href="/user/users">Users <span class="sr-only">(current)</span></a></li>
                    <li class="nav action"><a href="/date/date/?page=1">Date</a></li>
            <?php } ?>
                    <li class="nav action"><a href="/post/posts">Posts</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">

                    <li class="dropdown">
                        
                        <a href="/profile/get/<?=$_SESSION['user']['id']?>"  data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <!-- Profile image -->
                        <?php if(isset($_SESSION['user'])) : ?>
                            <img  src="<?=$_SESSION['user']['image']?>" alt="Profile image" class="img-circle"  width="30" height="30">
                            <!-- <?=$_SESSION['user']['first_name'];?> -->
                        <?php else: ?> 
                            <?=' User ';?>
                        <?php endif; ?>

                        <span class="caret"></span></a>  
                        <ul class="dropdown-menu">

                            <?php if(!isset($_SESSION['user'])) : ?>
                            <li><a href="/user/login">Login</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/user/register">Register</a></li>
                            <?php endif; ?>
                            
                            <?php if(isset($_SESSION['user'])) : ?>
                            <li><a href="/profile/get/<?=$_SESSION['user']['id']?>">Profile</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="/user/logout">Logout</a></li>
                            <?php endif; ?>

                        </ul>
                    </li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
</div>
<div class="container">
    <div class="row">
        <div class="col-sm-12">

            <?php require_once $content_page; ?>
        </div>
    </div>
</div>
    <div class="navbar-bottom row-fluid">
        <hr>   
        <p class="pull-left">Copyright © 2017</p>
        <p class="pull-right">Ростислав Лашин</p>
    </div> 

<!-- jQuery library -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

<script type="text/javascript" src="/web/js/script.js"></script>

</body>

</html