<?php


class FormValidator

{

	static protected $errors = [];

	static protected $massageErrors = [

		// Registratin form massages

        'email' => 'Введите свой Email',
        'first_name' => 'Ваше поле пустое, введите свое Имя',
        'last_name' => 'Ваше поле пустое, введите свою Фамилию',
        'first_name_4' => 'Ваше имя не насчитытвает 4-ти символов',
        'first_name_16' => 'Ваше имя привышает 16 символов',
        'last_name_4' => 'Ваша фамилия не насчитытвает 4-ти символов',
        'last_name_16' => 'Ваша фамилия привышает 16 символов',
        'password' => 'Ваши Пароли не совпадают',
        'password_8' => 'Пароль должен состовять не мение из 8-ми символов',

        // Login Form massages

        'email_not_exist' => 'Пользователя с таким Email не существует',
        'enter_password' => 'Вы ввели не верный пароль.',

        // Admin Form 

        'wrong_email' => ' Пользователь с таким Еmail уже существует.'
	];



	public static function clean($user) 

	{

		$user = array_map('trim', $user);
		$user = array_map('stripslashes', $user);
		$user = array_map('strip_tags', $user);
		$user = array_map('htmlspecialchars', $user);
	    
	    return $user;
	
	}

	function check_length($value = "", $min, $max) {

	    $result = (mb_strlen($value) < $min || mb_strlen($value) > $max);

	    return !$result;
	}



	public static function checkAdminForm($checkEmail, $trimmedUser)
	{ 

		if($checkEmail != empty($trimmedUser)){

			self::$errors[] = self::$massageErrors['wrong_email'];

		}

		if(empty($trimmedUser['email'])){

			self::$errors[] = self::$massageErrors['email'];

		}

		if(mb_strlen($trimmedUser['password']) < 8){

			self::$errors[] = self::$massageErrors['password_8'];

		}


		return self::getErrors();


	}

	public static function checkRegisterForm($checkEmail, $trimmedUser)
	{ 

		// cделать условие для email -> правельный набор

		if($checkEmail){

			self::$errors[] = self::$massageErrors['wrong_email'];

		}


		if(empty($trimmedUser['email'])){

			self::$errors[] = self::$massageErrors['email'];

		}

		if(empty($trimmedUser['first_name'])){

			self::$errors[] = self::$massageErrors['first_name'];

		}

		if(mb_strlen($trimmedUser['first_name']) < 3 ){

				self::$errors[] = self::$massageErrors['first_name_4'];
			}

		if(mb_strlen($trimmedUser['first_name']) > 16 ){

			self::$errors[] = self::$massageErrors['first_name_16'];

		}

		if(empty($trimmedUser['last_name'])){

			self::$errors[] = self::$massageErrors['last_name'];

		}

		if(mb_strlen($trimmedUser['last_name']) < 3){

			self::$errors[] = self::$massageErrors['last_name_4'];

		}
		if(mb_strlen($trimmedUser['last_name']) > 16){

			self::$errors[] = self::$massageErrors['last_name_16'];

		}

		if ($trimmedUser['password'] != $trimmedUser['confirm_password']) {

			self::$errors[] = self::$massageErrors['password'];

		}

		if(mb_strlen($trimmedUser['password']) < 8){

			self::$errors[] = self::$massageErrors['password_8'];

		}



		return self::getErrors();

	}



	public static function checkLoginForm($checkEmail, $trimmedUser)

	{ 

        if(!$checkEmail){

            self::$errors[] = self::$massageErrors['email_not_exist'];

        }

        if($checkEmail and $checkEmail['password'] != md5($trimmedUser['password'])){

            self::$errors[] = self::$massageErrors['enter_password'];

        }



		return self::getErrors();

	}




	   protected static function getErrors()

    {

        return self::$errors;

    }

}