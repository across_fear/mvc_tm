<?php


class ImageHelper
{
    public static function upload($newImage, $postId)
    {
        if (isset($newImage)) {
            $tmpName = $newImage['tmp_name'];
            $fileName = $newImage['name'];

            $imageLocation = ROOT . '/web/user_images/' . $fileName;
            $imagePath = '/web/user_images/' . $fileName;

            move_uploaded_file($tmpName, $imageLocation);

            $imageModel = new Image();
            $imageModel->save($imagePath, $postId);

        }
    }

        public static function uploadProfile($newImage)
    {

        if (isset($newImage)) {
            $tmpName = $newImage['tmp_name'];
            $fileName = $newImage['name'];

            $imageLocation = ROOT . '/web/profile_images/' . $fileName;
            $imagePath = '/web/profile_images/' . $fileName;

            move_uploaded_file($tmpName, $imageLocation);

            $imageModel = new Image();
            $imageModel->saveToProfile($imagePath);

        }
    }
}